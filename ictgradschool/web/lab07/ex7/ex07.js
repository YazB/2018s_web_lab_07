$(document).ready(function () {


    var numFem = 0;
    var numMale = 0;
    var num0to30 = 0;
    var num31to64 = 0;
    var num65onwards = 0;

    var numGold = 0;
    var numSilver = 0;
    var numBronze = 0;


//here is where I am getting the gender of the customers
    for (var i = 0; i < customers.length; i++) {
        if (customers[i].gender === "female") {
            numFem++;
        } else if (customers[i].gender === "male") {
            numMale++;
        }
    }
    //here is where I am getting the year of the customers
    for (var i = 0; i < customers.length; i++) {
        if ((2018 - customers[i].year_born) < 31) {
            num0to30++;
        } else if ((2018 - customers[i].year_born) > 31 && ((2018 - customers[i].year_born < 65))) {
            num31to64++;
        } else {
            if ((2018 - customers[i].year_born) > 65) {
                num65onwards++;
            }
        }
    }
    //here is where I am getting the loyalty tiers
    console.log("loyalty loop starts");
    for (var i = 0; i < customers.length; i++) {
        if ((customers[i].num_hires/((2018-customers[i].joined)*52))>= 4 ){
            numGold++;
        } else if ((customers[i].num_hires/((2018-customers[i].joined)*52))>1){
            numSilver++;
        } else if ((customers[i].num_hires/((2018-customers[i].joined*52)))<1 ){
            numBronze++;
        }
    }
    console.log("loyalty loop ends");

    //here is where I am building my table
    var table = document.getElementById("bodyone");
    for (var i = 0; i < customers.length; i++) {
        var row = table.insertRow(0);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var cell5 = row.insertCell(4);

        cell1.textContent = customers[i].name;
        cell2.textContent = customers[i].gender;
        cell3.textContent = customers[i].year_born;
        cell4.textContent = customers[i].joined;
        cell5.textContent = customers[i].num_hires;
    }

    row = table.insertRow();
    cell1 = row.insertCell(0);
    cell2 = row.insertCell(1);

    cell1.textContent = "This is the Number of Females:";
    cell2.textContent = numFem;

    row = table.insertRow();
    cell1 = row.insertCell(0);
    cell2 = row.insertCell(1);

    cell1.textContent = "This is the Number of Males:";
    cell2.textContent = numMale;

    row = table.insertRow();
    cell1 = row.insertCell(0);
    cell2 = row.insertCell(1);

    cell1.textContent = "This is the Number of Users under 30 years old:";
    cell2.textContent = num0to30;


    row = table.insertRow();
    cell1 = row.insertCell(0);
    cell2 = row.insertCell(1);

    cell1.textContent = "This is the Number of Users between 31 and 64 years old:";
    cell2.textContent = num31to64;

    row = table.insertRow();
    cell1 = row.insertCell(0);
    cell2 = row.insertCell(1);

    cell1.textContent = "This is the Number of Users over 65 years old:";
    cell2.textContent = num65onwards;

    row = table.insertRow();
    cell1 = row.insertCell(0);
    cell2 = row.insertCell(1);

    cell1.textContent = "This is the number of Gold members:";
    cell2.textContent = numGold;

    row = table.insertRow();
    cell1 = row.insertCell(0);
    cell2 = row.insertCell(1);

    cell1.textContent = "This is the number of Silver members:";
    cell2.textContent = numSilver;

    row = table.insertRow();
    cell1 = row.insertCell(0);
    cell2 = row.insertCell(1);

    cell1.textContent = "This is the number of Bronze members:";
    cell2.textContent = numBronze;
});