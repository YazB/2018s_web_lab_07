var d = new Date();


window.addEventListener("DOMContentLoaded", k);
var i;

function updateClock() {
    document.getElementById("exercisefour").textContent = d.getMilliseconds() + "milli-seconds" + "-" + d.getSeconds() + "seconds " + "-" + d.getMinutes() + "minutes" + "-" + d.getHours() + "-" + "hours";
    console.log("CLOCK WORKS");
}

function k() {
    i = setInterval(updateClock, 1000);
    console.log("INTERVAL WORKS");
}

document.addEventListener("DOMContentLoaded", function () {
    var stopper = document.getElementById("button1");
    stopper.addEventListener("click", function () {
        clearInterval(i);
        console.log("STOP");
    });
    var starter = document.getElementById("button2");
    starter.addEventListener("click", function () {
        k();
        console.log("START");
    });
});